package com.example.demo.repository.MotelCustomsImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dto.searchDto.MotelSearchClientDTO;
import com.example.demo.repository.MotelRepositoryCustoms;
import com.example.demo.util.DataUtil;
import com.example.demo.util.StringUtils;

@Repository
@Transactional
public class MotelRepositoryCustomsImpl implements MotelRepositoryCustoms {
@Autowired
    private  EntityManager entityManager;
	
	@Override
	public List<Object[]> searchMotelClient(MotelSearchClientDTO clientDTO) {
		 StringBuilder sql = new StringBuilder(
				 " 	select  m.id motelId, m.title,  m.description,m.motel_type,m.longitude,m.latitude,m.street , \r\n" + 
				 "	m.price_min , m.price_max ,m.acreage_min , m.acreage_max,m.deposits,m.uptop_status,m.last_uptop,m.contact_phone,m.contact_person,  \r\n" + 
				 "	m.live_with_host,m.private_toilet,m.number_bedroom,m.number_toilet,m.floor,m.number_floor,m.direction,m.apartment_situation,  \r\n" + 
				 "	m.length,m.width,m.count_view,m.created_date createdDteMotel , (select count(*) from report r where r.motel_id= m.id) countReport,  \r\n" + 
				 "	(select GROUP_CONCAT(pt.src)   from Photo pt where pt.motel_id = m.id ) photos ,m.status statusMotel , u.id userId,   \r\n" + 
				 "	m.wards,m.district,m.province , (select pt.src   from Photo  pt  where pt.motel_id = m.id  limit 1)  image from user u left join motel m on  u.id= m.user_id  where m.id is not null ");
		 			
		 			if(clientDTO.getProvince() !=null ) {
		 				sql.append(" and upper(m.province)  = :Province ");
		 			}
		 			if(clientDTO.getDistrict() !=null ) {
		 				sql.append(" and upper(m.district)  = :District ");
		 			}
		 			if(clientDTO.getWards() !=null ) {
		 				sql.append(" and upper(m.wards)  = :Wards ");
		 			}
		 			if(!DataUtil.isNullOrEmpty(clientDTO.getTypeMotel())) {
		 				sql.append(" and m.motel_type  In  (:TypeMotel) ");
		 			}
		 			
//		 			if(!DataUtil.isNullOrEmpty(clientDTO.getPriceMax())) {
//		 				sql.append(" and  m.price_max  >  :PriceMin ");
//		 			}
//		 			
//		 			if(!DataUtil.isNullOrEmpty(clientDTO.getPriceMin())) {
//		 				sql.append(" and m.price_min  <  :PriceMax  ");
//		 			}
//		 			
		 			
	 		sql.append(" order by m.uptop_status desc , m.created_date desc  ");

	 				
	 		        Query query = entityManager.createNativeQuery(sql.toString());

		 			if(clientDTO.getProvince() !=null ) {
	 				query.setParameter("Province", clientDTO.getProvince());
	 			}
		 			if(clientDTO.getDistrict() !=null) {
	 				query.setParameter("District", clientDTO.getDistrict());

	 			}
		 			if(clientDTO.getWards() !=null ) {
	 				query.setParameter("Wards", clientDTO.getWards());

	 			}
	 			if(!DataUtil.isNullOrEmpty(clientDTO.getTypeMotel())) {
	 				query.setParameter("TypeMotel", clientDTO.getTypeMotel());

	 			}
	 			
//	 			if(!DataUtil.isNullOrEmpty(clientDTO.getPriceMax())) {
//	 				query.setParameter("PriceMin", clientDTO.getPriceMax());
//
//	 			}
//	 			
//	 			if(!DataUtil.isNullOrEmpty(clientDTO.getPriceMin())) {
//	 				query.setParameter("PriceMax", clientDTO.getPriceMin());
//
//	 			}
		 
	 			List<Object[]> list = query.getResultList();
	 			
	 			
		return list;
	}

}
