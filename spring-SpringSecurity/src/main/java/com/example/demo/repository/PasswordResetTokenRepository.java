package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.PasswordResetToken;

@Repository
public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Long>{
	@Query( value = "SELECT p.* FROM Password_Reset_Token p WHERE p.token = :token", nativeQuery = true)
	PasswordResetToken findPasswordTokenForPasswordResetToken(@Param("token") String token);

	@Query ( value = "SELECT p.user_id FROM Password_Reset_Token p WHERE p.token = :token", nativeQuery = true)
	Long findUserIdBytoken(@Param("token") String token);
}
