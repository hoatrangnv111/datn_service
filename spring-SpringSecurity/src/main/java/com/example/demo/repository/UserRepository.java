package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.demo.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	Optional<User> findByUsername(String username);
	
	User findOneByUsername(String username);
	
	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);
	
	User findOneById(Long id);
	
	@Query(value = "SELECT u.* FROM User u WHERE u.email = :email", nativeQuery = true)
	User findUserByEmail(@Param("email") String email);

	@Query(value = "select u.id, u.username, u.fullname, u.email, u.status, group_concat(r.name), u.created_date " + 
			" From user u "
			+ " join user_role ur on u.id = ur.user_id "
			+ " join role r on r.id = ur.role_id "
			+ " where 1=1 "
			+ " and (:username is null or upper(u.username) like %:username% ) " 
			+ " and (:email is null or upper(u.email) like %:email% ) " 
			+ " and (:status = 0 or u.status = :status ) "
			+ " and (:roleID = 0 or ur.role_id = :roleID )" 
			+ " group by u.username"
			+ " order by u.username asc", 
			countQuery =  "select count(*) from"
					+ " (select  u.username From user u join user_role ur on u.id = ur.user_id " + 
					" where 1=1 "
					+ " and (:username is null or upper(u.username) like %:username% ) " 
					+ " and (:email is null or upper(u.email) like %:email% ) " 
					+ " and (:status = 0 or u.status = :status ) "
					+ " and (:roleID = 0 or ur.role_id = :roleID ) "
					+ " group by u.username "
					+ ") as T"
					,
					nativeQuery = true)
	Page<Object[]> doSearch(@Param("username") String username, @Param("email") String email,
			@Param("status") int status, @Param("roleID") int roleID
			,Pageable pageable
			);

}

	
