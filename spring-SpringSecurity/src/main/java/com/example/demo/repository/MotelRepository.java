package com.example.demo.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Motel;
@Repository
public interface MotelRepository extends MotelRepositoryCustoms, JpaRepository<Motel, Long> {

	
	@Query(value =
			" select u.id userId, u.username,u.email,u.fullname,u.phone,u.address addRessUser,u.created_date, m.id motelId, m.title,\r\n" + 
			" m.description,m.motel_type,m.longitude,m.latitude,concat(m.street,' , ',m.wards,' , ',m.district,' , ',m.province ) address,\r\n" + 
			" price_min , price_max ,acreage_min , acreage_max,m.deposits,m.uptop_status,m.last_uptop,m.contact_phone,m.contact_person,\r\n" + 
			" m.live_with_host,m.private_toilet,m.number_bedroom,m.number_toilet,m.floor,m.number_floor,m.direction,m.apartment_situation,\r\n" + 
			" m.length,m.width,m.count_view,m.created_date createdDteMotel , (select count(*) from report r where r.motel_id= m.id) countReport,\r\n" + 
			" (select GROUP_CONCAT(pt.src)   from Photo pt where pt.motel_id = m.id ) photos ,m.status statusMotel \r\n" + 
			" from user u left join motel m on  u.id= m.user_id  where m.id is not null "
			+ "      and  (:username is null or upper(u.username) like %:username%  )  " 
			+ " and (:motelType is null or upper(m.motel_type) = :motelType  )  "
			+ " and (:status =0 or m.status = :status  )  "
			+ " and (:province is null or upper(m.province) = :province  )  "
			+ " and (:district is null or upper(m.district) = :district  )  "
			+ " and (:wards is null or upper(m.wards) = :wards  )  ",
			countQuery = " select count(*) from  ( " +
					" select u.id userId, u.username,u.email,u.fullname,u.phone,u.address addRessUser,u.created_date, m.id motelId, m.title,\r\n" + 
					" m.description,m.motel_type,m.longitude,m.latitude,concat(m.street,' , ',m.wards,' , ',m.district,' , ',m.province ) address,\r\n" + 
					" price_min , price_max ,acreage_min , acreage_max,m.deposits,m.uptop_status,m.last_uptop,m.contact_phone,m.contact_person,\r\n" + 
					" m.live_with_host,m.private_toilet,m.number_bedroom,m.number_toilet,m.floor,m.number_floor,m.direction,m.apartment_situation,\r\n" + 
					" m.length,m.width,m.count_view,m.created_date createdDteMotel , (select count(*) from report r where r.motel_id= m.id) countReport,\r\n" + 
					" (select GROUP_CONCAT(pt.src)   from Photo pt where pt.motel_id = m.id ) photos ,m.status statusMotel \r\n" + 
					" from user u left join motel m on  u.id= m.user_id  where m.id is not null "
					+ "      and  (:username is null or upper(u.username) like %:username%  )  " 
					+ " and (:motelType is null or upper(m.motel_type) = :motelType  )  "
					+ " and (:status =0 or m.status = :status  )  "
					+ " and (:province is null or upper(m.province) = :province  )  "
					+ " and (:district is null or upper(m.district) = :district  )  "
					+ " and (:wards is null or upper(m.wards) = :wards  )  "
					+ " ) a  ", nativeQuery = true
		)
	Page<Object[]>  doSearch(@Param("username") String username,@Param("motelType") String motelType,@Param("status") int status,@Param("province") String province,
			@Param("district") String district, @Param("wards") String wards,Pageable pageable);
	
	
	
	@Query(value =
			" select  m.id motelId, m.title,\r\n" + 
					" m.description,m.motel_type,m.longitude,m.latitude,m.street ,\r\n" + 
					" price_min , price_max ,acreage_min , acreage_max,m.deposits,m.uptop_status,m.last_uptop,m.contact_phone,m.contact_person,\r\n" + 
					" m.live_with_host,m.private_toilet,m.number_bedroom,m.number_toilet,m.floor,m.number_floor,m.direction,m.apartment_situation,\r\n" + 
					" m.length,m.width,m.count_view,m.created_date createdDteMotel , (select count(*) from report r where r.motel_id= m.id) countReport,\r\n" + 
					" (select GROUP_CONCAT(pt.src)   from Photo pt where pt.motel_id = m.id ) photos ,m.status statusMotel , u.id userId, \r\n" + 
					"  m.wards,m.district,m.province ,(select pt.src   from Photo pt where pt.motel_id = m.id LIMIT 1 ) image  "
					+ " from user u left join motel m on  u.id= m.user_id  where m.id is not null "
					+ "      and  (:userId is null or u.id =:userId  )  " ,
			countQuery = " select count(*) from  ( " +
					" select  m.id motelId, m.title,\r\n" + 
					" m.description,m.motel_type,m.longitude,m.latitude,m.street ,\r\n" + 
					" price_min , price_max ,acreage_min , acreage_max,m.deposits,m.uptop_status,m.last_uptop,m.contact_phone,m.contact_person,\r\n" + 
					" m.live_with_host,m.private_toilet,m.number_bedroom,m.number_toilet,m.floor,m.number_floor,m.direction,m.apartment_situation,\r\n" + 
					" m.length,m.width,m.count_view,m.created_date createdDteMotel , (select count(*) from report r where r.motel_id= m.id) countReport,\r\n" + 
					" (select GROUP_CONCAT(pt.src)   from Photo pt where pt.motel_id = m.id ) photos ,m.status statusMotel , u.id userId, \r\n" + 
					"  m.wards,m.district,m.province  (select pt.src   from Photo pt  where pt.motel_id = m.id  LIMIT 1) image "
					+ " from user u left join motel m on  u.id= m.user_id  where m.id is not null "
					+ "      and  (:userId is null or u.id =:userId  )  " 
			
					+ " ) a  ", nativeQuery = true
		)
	Page<Object[]>  doSearchUser(@Param("userId") String userId,Pageable pageable);
	
	
}
