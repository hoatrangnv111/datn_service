package com.example.demo.services;

import java.nio.file.Path;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface IFileStorageService {
	public void init();
	public String save(MultipartFile file, String username, String folder);
	
	public Resource load(String filename, String folder);
	
	public void deleteAll();
	
	public Stream<Path> loadAll();
}
