package com.example.demo.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.dto.MotelDTO;
import com.example.demo.dto.searchDto.MotelResponDTO;
import com.example.demo.dto.searchDto.MotelSearchClientDTO;
import com.example.demo.dto.searchDto.MotelSearchDTO;
import com.example.demo.entity.Motel;
import com.example.demo.entity.Photo;
import com.example.demo.repository.MotelRepository;
import com.example.demo.repository.PhotoRepository;
import com.example.demo.services.MotelService;
import com.example.demo.util.DataUtil;
@Service
public class MotelServiceImpl implements MotelService {
@Autowired
private MotelRepository motelRepository;
 @Autowired
 private MotelDTO converDto;
 @Autowired PhotoRepository photoRepository;

public MotelResponDTO convertMotelResponDTO(Object[] obj) {
	MotelResponDTO dto = new MotelResponDTO();
	dto.setUserId(DataUtil.safeToLong(obj[0]));
	dto.setUserName(DataUtil.safeToString(obj[1]));
	dto.setEmail(DataUtil.safeToString(obj[2]));
	dto.setFullName(DataUtil.safeToString(obj[3]));
	dto.setPhone(DataUtil.safeToString(obj[4]));;
	dto.setAddRessUser(DataUtil.safeToString(obj[5]));
	dto.setCreatedDateUser(DataUtil.safeToString(obj[6]));
	dto.setMotelId(DataUtil.safeToLong(obj[7]));
	dto.setTitle(DataUtil.safeToString(obj[8]));
	dto.setDescription(DataUtil.safeToString(obj[9]));
	dto.setMotelType(DataUtil.safeToInt(obj[10]));
	dto.setLongitude(DataUtil.safeToString(obj[11]));
	dto.setLatitude(DataUtil.safeToString(obj[12]));
	dto.setAddressMotel(DataUtil.safeToString(obj[13]));
	dto.setPriceMin(DataUtil.safeToDouble(obj[14]));
	dto.setPriceMax(DataUtil.safeToDouble(obj[15]));
	dto.setAcreageMin(DataUtil.safeToDouble(obj[16]));
	dto.setAcreageMax(DataUtil.safeToDouble(obj[17]));
	dto.setDeposits(DataUtil.safeToDouble(obj[18]));;
	dto.setUptopStatus(DataUtil.safeToInt(obj[19]));
	dto.setLastUptop(DataUtil.safeToString(obj[20]));
	dto.setContactPhone(DataUtil.safeToString(obj[21]));
	dto.setContactPerson(DataUtil.safeToString(obj[22]));
	dto.setLiveWithHost(DataUtil.safeToInt(obj[23]));
	dto.setPrivateToilet(DataUtil.safeToInt(obj[24]));
	dto.setNumberBedroom(DataUtil.safeToInt(obj[25]));
	dto.setNumberToilet(DataUtil.safeToInt(obj[26]));
	dto.setFloor(DataUtil.safeToInt(obj[27]));
	dto.setNumberFloor(DataUtil.safeToInt(obj[28]));
	dto.setDirection(DataUtil.safeToInt(obj[29]));
	dto.setApartmentSituation(DataUtil.safeToInt(obj[30]));
	dto.setLength(DataUtil.safeToDouble(obj[31]));
	dto.setWidth(DataUtil.safeToDouble(obj[32]));
	dto.setCountView(DataUtil.safeToInt(obj[33]));
	dto.setCreatedDateMotel(DataUtil.safeToString(obj[34]));
	dto.setCountReport(DataUtil.safeToString(obj[35]));
	dto.setPhotos(DataUtil.safeToString(obj[36]));
	dto.setStatusMotel(DataUtil.safeToInt(obj[37]));
	return dto;
}


public MotelResponDTO convertMotelUserResponDTO(Object[] obj) {
	MotelResponDTO dto = new MotelResponDTO();
	
	dto.setMotelId(DataUtil.safeToLong(obj[0]));
	dto.setTitle(DataUtil.safeToString(obj[1]));
	dto.setDescription(DataUtil.safeToString(obj[2]));
	dto.setMotelType(DataUtil.safeToInt(obj[3]));
	dto.setLongitude(DataUtil.safeToString(obj[4]));
	dto.setLatitude(DataUtil.safeToString(obj[5]));
	dto.setStreet(DataUtil.safeToString(obj[6]));
	dto.setPriceMin(DataUtil.safeToDouble(obj[7]));
	dto.setPriceMax(DataUtil.safeToDouble(obj[8]));
	dto.setAcreageMin(DataUtil.safeToDouble(obj[9]));
	dto.setAcreageMax(DataUtil.safeToDouble(obj[10]));
	dto.setDeposits(DataUtil.safeToDouble(obj[11]));;
	dto.setUptopStatus(DataUtil.safeToInt(obj[12]));
	dto.setLastUptop(DataUtil.safeToString(obj[13]));
	dto.setContactPhone(DataUtil.safeToString(obj[14]));
	dto.setContactPerson(DataUtil.safeToString(obj[15]));
	dto.setLiveWithHost(DataUtil.safeToInt(obj[16]));
	dto.setPrivateToilet(DataUtil.safeToInt(obj[17]));
	dto.setNumberBedroom(DataUtil.safeToInt(obj[18]));
	dto.setNumberToilet(DataUtil.safeToInt(obj[19]));
	dto.setFloor(DataUtil.safeToInt(obj[20]));
	dto.setNumberFloor(DataUtil.safeToInt(obj[21]));
	dto.setDirection(DataUtil.safeToInt(obj[22]));
	dto.setApartmentSituation(DataUtil.safeToInt(obj[23]));
	dto.setLength(DataUtil.safeToDouble(obj[24]));
	dto.setWidth(DataUtil.safeToDouble(obj[25]));
	dto.setCountView(DataUtil.safeToInt(obj[26]));
	dto.setCreatedDateMotel(DataUtil.safeToString(obj[27]));
	dto.setCountReport(DataUtil.safeToString(obj[28]));
	dto.setPhotos(DataUtil.safeToString(obj[29]));
	dto.setStatusMotel(DataUtil.safeToInt(obj[30]));
	dto.setUserId(DataUtil.safeToLong(obj[31]));
	dto.setWards(DataUtil.safeToString(obj[32]));
	dto.setDistrict(DataUtil.safeToString(obj[33]));
	dto.setProvince(DataUtil.safeToString(obj[34]));
	dto.setImage(DataUtil.safeToString(obj[35]));
	return dto;
}


	@Override
	public Page<MotelResponDTO> doSearch(MotelSearchDTO dto,Pageable pageable) { 
		Page<MotelResponDTO>  a = motelRepository.doSearch(dto.getUsername(),
			dto.getMotelType(), dto.getStatus(), dto.getProvince(), dto.getDistrict(),dto.getWards(),	pageable).map(this::convertMotelResponDTO);
		return a;
	}


	@Override
	public Page<MotelResponDTO> doSearchUser(String dto, Pageable pageable) {
		Page<MotelResponDTO>  a = motelRepository.doSearchUser(dto,	pageable).map(this::convertMotelUserResponDTO);
			return a;
	}


	@Override
	public String save(MotelDTO dto) {
		
		
if(dto.getMotelId()!=null) {
	Motel motel = motelRepository.save(dto.toDoMotel(dto));
	if(motel!=null) {
		photoRepository.deleteAllByModelId(dto.getMotelId());
		if(dto.getImage1()!=null) {
			Photo photo1 =new Photo();
			photo1.setMotel_id(motel.getId());
			photo1.setSrc(dto.getImage1());
			photoRepository.save(photo1);
		}
		if(dto.getImage2()!=null) {
			Photo photo1 =new Photo();
			photo1.setMotel_id(motel.getId());
			photo1.setSrc(dto.getImage2());
			photoRepository.save(photo1);
		}
		if(dto.getImage3()!=null) {
			Photo photo1 =new Photo();
			photo1.setMotel_id(motel.getId());
			photo1.setSrc(dto.getImage3());
			photoRepository.save(photo1);
		}
		if(dto.getImage4()!=null) {
			Photo photo1 =new Photo();
			photo1.setMotel_id(motel.getId());
			photo1.setSrc(dto.getImage4());
			photoRepository.save(photo1);
		}
		return "Cập nhật bài đăng thành công";
	}
}else {
	Motel motel = motelRepository.save(dto.toDoMotel(dto));
	
	if(motel!=null) {
		if(dto.getImage1()!=null) {
			Photo photo1 =new Photo();
			photo1.setMotel_id(motel.getId());
			photo1.setSrc(dto.getImage1());
			photoRepository.save(photo1);
		}
		if(dto.getImage2()!=null) {
			Photo photo1 =new Photo();
			photo1.setMotel_id(motel.getId());
			photo1.setSrc(dto.getImage2());
			photoRepository.save(photo1);
		}
		if(dto.getImage3()!=null) {
			Photo photo1 =new Photo();
			photo1.setMotel_id(motel.getId());
			photo1.setSrc(dto.getImage3());
			photoRepository.save(photo1);
		}
		if(dto.getImage4()!=null) {
			Photo photo1 =new Photo();
			photo1.setMotel_id(motel.getId());
			photo1.setSrc(dto.getImage4());
			photoRepository.save(photo1);
		}
			
			return "Thêm bài đăng thành công";
		}

}
	
		return null;
	}


	@Override
	public List<MotelResponDTO> searchClient(MotelSearchClientDTO clientDTO) {
		List<MotelResponDTO> result= new ArrayList<MotelResponDTO>();
		for (Object[] obj : motelRepository.searchMotelClient(clientDTO)) {
			MotelResponDTO  dto = convertMotelUserResponDTO(obj);
			result.add(dto);
		}
		return result;
	}

}
