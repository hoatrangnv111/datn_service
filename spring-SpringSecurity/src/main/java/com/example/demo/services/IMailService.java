package com.example.demo.services;

import org.springframework.mail.SimpleMailMessage;

import com.example.demo.entity.User;


public interface IMailService {
	SimpleMailMessage constructEmail(String subject, String body, User user);
	SimpleMailMessage constructResetTokenEmail(String contextPath, String token, User user, String message);
}
