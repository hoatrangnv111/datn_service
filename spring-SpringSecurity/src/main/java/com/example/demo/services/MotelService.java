package com.example.demo.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.dto.MotelDTO;
import com.example.demo.dto.searchDto.MotelResponDTO;
import com.example.demo.dto.searchDto.MotelSearchClientDTO;
import com.example.demo.dto.searchDto.MotelSearchDTO;
import com.example.demo.entity.Motel;


public interface MotelService {
Page<MotelResponDTO> doSearch(MotelSearchDTO dto,Pageable pageable);
Page<MotelResponDTO> doSearchUser(String dto,Pageable pageable);
String save(MotelDTO motel);
List<MotelResponDTO> searchClient(MotelSearchClientDTO clientDTO);
}
