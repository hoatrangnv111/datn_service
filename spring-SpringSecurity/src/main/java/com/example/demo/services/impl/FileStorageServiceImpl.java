package com.example.demo.services.impl;

import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.services.IFileStorageService;

@Service
public class FileStorageServiceImpl implements IFileStorageService{
	
	private Path root = Paths.get("upload");
	public void getPath(String folder) {
		root = Paths.get("upload/"+folder);
	}
	@Override
	public void init() {
		// TODO Auto-generated method stub
	}
	@Override
	public String save(MultipartFile file, String username, String folder) {
		this.getPath(folder);
		try {
			String filename = username+"."+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
			Files.deleteIfExists(this.root.resolve(filename));
			Files.copy(file.getInputStream(), this.root.resolve(filename));
			return filename;
		} catch (Exception e) {
			throw new RuntimeException("Could not store the file. Error: "+ e.getMessage());
		}
		
	}
	@Override
	public Resource load(String filename, String folder) {
		this.getPath(folder);
		try {
			Path file = root.resolve(filename);
			Resource resource = new UrlResource(file.toUri());
			
			if( resource.exists() || resource.isReadable()) {
				return resource;
			}else {
				throw new RuntimeException("Could not read the file.");
			}
		} catch (MalformedURLException e) {
			throw new RuntimeException("Error: "+e.getMessage());
		}
	}
	

	

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Stream<Path> loadAll() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	
}
