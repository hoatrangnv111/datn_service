package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table ( name = "payment_motel")
@EntityListeners(AuditingEntityListener.class)
public class PaymentMotel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column ( name = "id")
	private Long id;
	@Column ( name = "user_id")
	private Long user_id;
	@Column ( name = "motel_id")
	private Long motel_id;
	
	@CreatedBy
	@Column ( name = "created_by")
	private String created_by;
	@CreatedDate
	@Column ( name = "created_date")
	private Date created_date;
	
	@Column ( name = "total_money")
	private double total_money;
}
