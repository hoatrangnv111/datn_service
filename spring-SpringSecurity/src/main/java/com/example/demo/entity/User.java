package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@Entity
@Table( name = "user",
uniqueConstraints = {
		@UniqueConstraint(columnNames = "username"),
		@UniqueConstraint(columnNames = "email")
}
)
@EntityListeners(AuditingEntityListener.class)
public class User {
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "username", length = 18)
	private String username;
	@Column(name = "password")
	private String password;
	@Column(name = "email", length = 100)
	private String email;
	@Column(name = "fullname", length = 50)
	private String fullname;
	@Column(name = "phone", length = 15)
	private String phone;
	
	@Column(name = "birthday")
	private Date birthday;
	@Column( name = "gender")
	private int gender;
	@Column(name = "avatar")
	private String avatar;
	@Column(name = "address")
	private String address;
	@Column ( name = "balance")
	private double balance;
	
	@Column( name = "status", nullable = false)
	private int status;
	@CreatedDate
	@Column ( name = "created_date")
	private Date created_date;
	@LastModifiedDate
	@Column ( name = "modified_date")
	private Date modified_date;
	@LastModifiedBy
	@Column ( name = "modified_by", length = 18)
	private String modified_by;
	
	public User(String username, String password, String email, String fullname, Date birthDay, String address, String phone, String avatar, int status, int gender) {
		this.username = username;
		this.password = password;
		this.email = email;
		this.fullname = fullname;
		this.birthday = birthDay;
		this.address = address;
		this.phone = phone;
		this.avatar = avatar;
		this.status = status;
		this.gender = gender;
	}

	
	

	
	
	
}
