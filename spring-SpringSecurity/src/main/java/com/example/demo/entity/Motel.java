package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@Entity
@Table( name = "motel")
@EntityListeners(AuditingEntityListener.class)
public class Motel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column( name = "id")
	private Long id;
	@Column ( name = "user_id")
	private Long user_id;
	@Column ( name = "title", length = 70)
	private String title;
	@Column ( name = "description", length = 255)
	private String description;
	@Column ( name = "motel_type")
	private Integer motel_type;
	
	
	@Column ( name = "longitude", length = 50)
	private String longitude;
	@Column ( name = "latitude", length = 50)
	private String latitude;
	@Column( name = "price_min")
	private Double price_min;
	@Column( name = "price_max")
	private Double price_max;
	@Column( name = "acreage_min")
	private Double acreage_min;
	@Column( name = "acreage_max")
	private Double acreage_max;
	@Column ( name = "deposits", length = 50)
	private String deposits;
	
	@Column ( name = "uptop_status")
	private Integer uptop_status;
	@Column( name = "last_uptop")
	private Date last_uptop;
	@Column ( name = "contact_phone")
	private String contact_phone;
	@Column ( name = "contact_person")
	private String contact_person;
	
	@Column ( name = "province")
	private String province;
	@Column ( name = "district")
	private String district;
	@Column ( name = "wards")
	private String wards;
	@Column ( name = "street")
	private String street;
	
	@Column ( name = "live_with_host")
	private Integer live_with_host;
	@Column ( name = "private_toilet")
	private Integer private_toilet;
	@Column ( name = "number_bedroom")
	private Integer number_bedroom;
	@Column ( name = "number_toilet")
	private Integer number_toilet;
	@Column ( name = "floor")
	private Integer floor;
	@Column ( name = "number_floor")
	private Integer number_floor;
	@Column ( name = "direction")
	private Integer direction;
	@Column ( name = "apartment_situation")
	private Integer apartment_situation;
	@Column ( name = "length")
	private Double length;
	@Column ( name = "width")
	private Double width;
	
	
	@Column ( name = "status")
	private Integer status;
	@Column ( name = "count_report")
	private Integer count_report;
	@Column ( name = "count_view")
	private Integer count_view;
	@CreatedBy
	@Column ( name = "created_by")
	private String created_by;
	@CreatedDate
	@Column ( name = "created_date")
	private Date created_date;
	@LastModifiedBy
	@Column ( name = "modified_by")
	private String modified_by;
	@LastModifiedDate
	@Column ( name = "modified_date")
	private Date modified_date;
	
}
