package com.example.demo.helper;

import org.springframework.web.multipart.MultipartFile;

public class ImageHelper {
	static String[] TYPES = {"image/jpeg", "image/png", "image/svg+xml"};
	public static boolean hasImageFormat( MultipartFile file) {
		for ( String type : TYPES) {
			if(file.getContentType().equals(type)) {
				return true;
			}
		}
		return false;
	}
}
