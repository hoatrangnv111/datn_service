package com.example.demo.dto;

import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor

public class ResultDataDTO {
	private Long id;
	private String username;
	private String fullname;
	private String email;
	private int status;
	private String rolename;
	private Date created_date;
	
}
