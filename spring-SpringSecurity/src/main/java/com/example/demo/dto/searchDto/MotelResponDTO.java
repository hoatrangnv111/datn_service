package com.example.demo.dto.searchDto;

import com.example.demo.entity.ERole;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class MotelResponDTO {
	
	private Long userId;
	private String userName;
	private String email;
	private String fullName;
	private String phone;
	private String addRessUser;
	private String createdDateUser;
	private Long motelId;
	private String title;
	private String description;
	private Integer motelType;
	private String longitude;
	private String latitude;
	private String addressMotel;
	private Double priceMin;
	private Double priceMax;
	private Double acreageMin;
	private Double acreageMax;
	private Double deposits;
	private Integer uptopStatus;
	private String lastUptop;
	private String contactPhone;
	private String contactPerson;
	private Integer liveWithHost;
	private Integer privateToilet;
	private Integer numberBedroom;
	private Integer numberToilet;
	private Integer floor;
	private Integer numberFloor;
	private Integer direction;
	private Integer apartmentSituation;
	private Double length;
	private Double width;
	private Integer countView;
	private String createdDateMotel;
	private String countReport;
	private String photos;
	private Integer statusMotel;
	private String district;
	private String wards;
	private String province;
	private String street;
	private String image;
	
	
	
	
	

}
